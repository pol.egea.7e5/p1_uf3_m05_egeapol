﻿using System;

namespace P1_UF3_M05_EgeaPol
{
    /// <summary>
    /// Classe d'inicialització
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Mètode que fa executar el menú
        /// </summary>
        public static void Main()
        {
            do MostraMenu();
            while (!TractaOpcio());
        }
        /// <summary>
        /// Mètode que mostra el menú
        /// </summary>
        public static void MostraMenu()
        {
            Console.WriteLine("1-Exercici2");
            Console.WriteLine("2-Exercici3");
            Console.WriteLine("3-Exercici4");
            Console.WriteLine("0-FI");
        }
        /// <summary>
        /// Mètode que tria l'opció del menú
        /// </summary>
        /// <returns>Retorna true si vol sortir del programa, false en cas contrari</returns>
        public static bool TractaOpcio()
        {
            string num = Console.ReadLine();
            switch (num)
            {
                case "1":
                    Exercici2();
                    return false;
                case "2":
                    Exercici3();
                    return false;
                case "3":
                    Exercici4();
                    return false;
            }

            return true;
        }
        /// <summary>
        /// Mètode per al exercici 1-2, la definició i entrada i sortida de dades.
        /// </summary>
        public static void Exercici2()
        {
            int[] centre = new int[] {1, 0};
            var cercle1 = new Cercle(6.0,centre);
            int[] centre2 = new int[] {5, 3};
            var cercle2 = new Cercle(3.5,centre2);
            int[] centre3 = new int[] {10,9};
            var cercle3 = new Cercle(8.1,centre3);
            Console.WriteLine(cercle1);
            Console.WriteLine(cercle2);
            Console.WriteLine(cercle3);
            //set radi
            Console.WriteLine("1r Exercici");
            cercle1.SetRadi(456);
            Console.WriteLine(cercle1);
            //set centre
            Console.WriteLine("2n Exercici");
            centre2 = new[] {103,2};
            cercle2.SetCentre(centre2);
            Console.WriteLine(cercle2);
            //get area
            Console.WriteLine("3r Exercici, l'àrea és el valor del final");
            Console.WriteLine(cercle3);
            //tots retornen l'area
        }
        /// <summary>
        /// Mètode per al exercici 3, la definició i entrada i sortida de dades.
        /// </summary>
        public static void Exercici3()
        {
            var costella = new PaquetCarn1(800, "Costella", 45);
            var pit = new PaquetCarn1(1200, "Pit", 28);
            var morro = new PaquetCarn1(300, "Morro", 23);
            Console.WriteLine(costella);
            Console.WriteLine(pit);
            Console.WriteLine(morro);
            Console.WriteLine("Pujem IVA");
            PaquetCarn1.FixarIva(15);
            Console.WriteLine(costella.PreuAmbIva());
            Console.WriteLine(pit.PreuAmbIva());
            Console.WriteLine(morro.PreuAmbIva());

        }
        ///<summary>
        /// Mètode per al exercici 4, la definició i entrada i sortida de dades.
        /// </summary>
        public static void Exercici4()
        {
            var costella = new PaquetCarn2(800, "Costella", 45);
            var pit = new PaquetCarn2(1200, "Pit", 28);
            var morro = new PaquetCarn2(300, "Morro", 23);
            Console.WriteLine(costella);
            Console.WriteLine(pit);
            Console.WriteLine(morro);
            Console.WriteLine("Pujem IVA");
            PaquetCarn2.FixarIva(15);
            Console.WriteLine(costella);
            Console.WriteLine(pit);
            Console.WriteLine(morro);
        }
    }
}