namespace P1_UF3_M05_EgeaPol
{
    /// <summary>
    /// Classe per al exercici 1-2
    /// </summary>
    public class Cercle
    {
        /// <summary>
        /// Atribut del cercle, radi d'aquest.
        /// </summary>
        private double _radi;
        /// <summary>
        /// Atribut de l'objecte, punt central d'aquest
        /// </summary>
        private int[] _centre;
        /// <summary>
        /// Atribut de la circumferència, àrea total
        /// </summary>
        private double _area;
        /// <summary>
        /// Mètode d'introducció dels atributs
        /// </summary>
        /// <param name="radi">Variable double, radi del cercle</param>
        /// <param name="centre">Variable array de ints, coordenades d'un vector</param>
        public Cercle(double radi, int[] centre)
        {
            _radi = radi;
            _centre = centre;
            _area=CalculArea(_radi);
        }
        /// <summary>
        /// Mètode de sortida de dades de l'objecte
        /// </summary>
        /// <returns>Retorna les dades del objecte</returns>
        public override string ToString()
        {
            return _radi + "\t" + $"x={_centre[0]},y={_centre[1]}" + "\t" + _area;
        }
        /// <summary>
        /// Mètode de modificació de radi
        /// </summary>
        /// <param name="radi">Valor nou del radi</param>
        public void SetRadi(double radi)
        {
            _radi = radi;
            _area=CalculArea(_radi);
        }
        /// <summary>
        /// Mètode de modificació del centre
        /// </summary>
        /// <param name="centre">Valors nous del centre</param>
        public void SetCentre(int[] centre)
        {
            _centre = centre;
        }
        /// <summary>
        /// Mètode de càlcul de l'àrea
        /// </summary>
        /// <param name="radi">Valor del radi per a calcular l'àrea</param>
        public double CalculArea(double radi)
        {
            return 3.1415*(radi*radi);
        }
    }
}