namespace P1_UF3_M05_EgeaPol
{
    /// <summary>
    /// Classe per al exercici4
    /// </summary>
    public class PaquetCarn2
    {
        /// <summary>
        /// Atribut del producte, pes enter
        /// </summary>
        public int _pes;
        /// <summary>
        /// Atribut del producte, part de la carn
        /// </summary>
        public string _part ;
        /// <summary>
        /// Atribut del producte, preu sense IVA
        /// </summary>
        public decimal _preuSenseIva;
        /// <summary>
        /// Atribut stricte de tots els productes, IVA aplicat (percentatge)
        /// </summary>
        protected static int TasaIva = 7;
        /// <summary>
        /// Atribut del producte,preu amb IVA aplicat.
        /// </summary>
        public decimal PreuAmbIva
        {
            get { return _preuSenseIva + (_preuSenseIva / 100 * TasaIva); }
        }
        /// <summary>
        /// Mètode per a la introducció dels valors als atributs
        /// </summary>
        /// <param name="pes">Variable entera, pes del producte</param>
        /// <param name="part">Variable string, part de carn del producte</param>
        /// <param name="preuSenseIva">Variable decimal, preu sense IVA del producte</param>
        public PaquetCarn2(int pes, string part, decimal preuSenseIva)
        {
            _pes = pes;
            _part = part;
            _preuSenseIva=preuSenseIva;
        }
        /// <summary>
        /// Mètode per la sortida de dades del objecte
        /// </summary>
        /// <returns>Retorna els valors del objecte</returns>
        public override string ToString()
        {
            return _pes + "\t" + _part + "\t" + _preuSenseIva+"\t"+PreuAmbIva;
        }
        /// <summary>
        /// Mètode per a fixar l'IVA en el conjunt dels productes.
        /// </summary>
        /// <param name="novaTasa"></param>
        public static void FixarIva(int novaTasa)
        {
            TasaIva = novaTasa;
        }

        
        
    }
}